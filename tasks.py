class Task1:

    def __init__(self, french, pianists, swimmers):
        self.french = french
        self.pianists = pianists
        self.swimmers = swimmers

    def special_students(self):
        """swimmers-pianists not learning French"""
        return list(set(self.swimmers).intersection(set(self.pianists)).difference(set(self.french)))


class Task2:

    def __init__(self, list_1, list_2):
        self.list_1 = list_1
        self.list_2 = list_2

    def get_unique_list_1(self):
        """return unique elements of the first list"""
        return list(set(self.list_1))

    def get_unique_both_lists(self):
        """return unique elements of the both lists"""
        return list(set(self.list_1).union(set(self.list_2)))


class Task3:

    def __init__(self, films):
        self.films = films

    def get_results(self):
        """return dict with counts"""
        res = {}
        for i in self.films:
            res[i] = self.films.count(i)
        return dict(sorted(res.items(), key=lambda x: x[1], reverse=True))


class Task4:

    def __init__(self, text):
        self.text = text

    def word_counter(self):
        """word counter dict in descending order"""
        import re
        no_punc = re.sub(r'[^\w\s]', '', self.text).lower().split()
        res = {}
        for i in no_punc:
            res[i] = no_punc.count(i)
        return dict(sorted(res.items(), key=lambda x: x[1], reverse=True))


class Task5:

    def __init__(self, family):
        self.family = family

    def parents(self, name):
        """ returns list of parents """
        return self.family[name]

    def grandparents(self, name):
        """ returns list of grandmothers and grandfathers """
        gp = []
        for x in self.family[name]:
            for g in self.family[x]:
                gp.append(g)
        return gp

    def siblings(self, name):
        """ returns list of siblings """
        s = []
        for k, v in self.family.items():
            if v == self.family[name] and k != name:
                s.append(k)
        return s

    def children(self, name):
        """ returns list of children """
        c = []
        for k, v in self.family.items():
            for x in v:
                if x == name:
                    c.append(k)
        return c

    def grandchildren(self, name):
        """ returns list of grandchildren """
        c = []
        gc = []
        for k, v in self.family.items():
            for x in v:
                if x == name:
                    c.append(k)
        for k, v in self.family.items():
            for x in v:
                if x in c:
                    gc.append(k)
        return gc
