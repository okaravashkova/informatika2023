class Mother:
    def __init__(self, age, name):
        self.age = age
        self.name = name
    def __str__(self):
        return '[Mothers name: %s]' % self.name


class Daughter(Mother):
    def __str__(self):                        
        return '[Daughters name: %s]' % self.name

mum = Mother(16, "Maria")
child = Daughter(0, 'Jesus')

print(mum)
print(child)
