# Реализйте класс Animal. Внутри объявите поле для имени и возраста. 
# От класса Animal унаследуйте класс Zebra и Dolphin.
# Оба класса должны возвращать описание, содержащее имя, возраст и вид животного

class Animal:
    def __init__(self, age, name):
        self.age = age
        self.name = name

class Zebra(Animal):
    def display(self):
        print(self.name, self.age, 'zebra')
    def species(self):
        self.species = 'zebra'

class Dolphin(Animal):
    def display(self):
        print(self.name, self.age, 'dolphin')

zebra = Zebra(17, 'Martin')
delphin = Dolphin(24, 'Bella')

delphin.display()
zebra.display()